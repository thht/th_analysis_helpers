import mne
import autoreject
import numpy as np
from collections import namedtuple

AutorejectReturn = namedtuple('AutorejectReturn', [
    'bad_chans',
    'annotations',
    'ar',
    'reject_log',
    'rejection_dict'
])


def _get_bad_chans_ransac():
    pass


def _get_bad_chans_maxfilter():
    pass


def get_bad_channels_and_timewindows(raw_data, use_bad_chans_maxfilter=True,
                                     n_jobs=1,
                                     use_global_threshold=False,
                                     bad_chans_maxwell_kwargs=None,
                                     ar_kwargs=None):
    if bad_chans_maxwell_kwargs is None:
        bad_chans_maxwell_kwargs = dict()

    if ar_kwargs is None:
        ar_kwargs = dict()

    art_events = mne.make_fixed_length_events(raw_data, duration=1)

    epochs_for_art = mne.Epochs(raw_data, events=art_events,
                                tmin=0, tmax=1,
                                proj=True, reject=None, preload=True,
                                baseline=None, detrend=0)

    if use_bad_chans_maxfilter:
        noise_chs, flat_chs = mne.preprocessing.find_bad_channels_maxwell(
            raw_data, **bad_chans_maxwell_kwargs
        )

        bad_chans = noise_chs + flat_chs
    else:
        bad_chans = list()
        ransac = autoreject.Ransac(n_jobs,
                                   verbose='tqdm')
        ch_types = ['mag', 'grad']
        for cur_ch_type in ch_types:
            tmp_epochs = epochs_for_art.copy()
            tmp_epochs.pick_types(meg=cur_ch_type)
            tmp_epochs.info.normalize_proj()

            ransac.fit(tmp_epochs)
            bad_chans.extend(ransac.bad_chs_)

    epochs_for_art.drop_channels(bad_chans)

    rejection_dict = None
    reject_log = None
    ar = None

    if use_global_threshold:
        rejection_dict = autoreject.get_rejection_threshold(epochs_for_art,
                                                            verbose='tqdm')
        epochs_rejected = epochs_for_art.copy()
        epochs_rejected.drop_bad(rejection_dict)
        bad_epochs_idx = list()
        for idx, rej in enumerate(epochs_rejected.drop_log):
            if rej and idx < len(epochs_for_art):
                bad_epochs_idx.append(idx)
        bad_epochs = epochs_for_art[bad_epochs_idx]
    else:
        ar = autoreject.AutoReject(n_jobs=n_jobs,
                                   verbose='tqdm',
                                   **ar_kwargs)

        ar.fit(epochs_for_art)
        reject_log = ar.get_reject_log(epochs_for_art)

        bad_epochs = epochs_for_art[reject_log.bad_epochs]

    annot_onsets = (bad_epochs.events[:, 0] - raw_data.first_samp) / \
                   raw_data.info['sfreq']
    annot_durations = np.ones(annot_onsets.shape)

    annotations = mne.Annotations(annot_onsets, annot_durations,
                                  'bad autoreject')


    rval = AutorejectReturn(
        bad_chans=bad_chans,
        annotations=annotations,
        ar=ar,
        reject_log=reject_log,
        rejection_dict=rejection_dict
    )

    return rval