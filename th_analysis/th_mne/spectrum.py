from copy import deepcopy

import mne
import numpy as np
from mne.channels.channels import ContainsMixin, UpdateChannelsMixin
from mne.utils import SizeMixin, sizeof_fmt
from mne.viz.utils import plt_show


class Spectrum(ContainsMixin, UpdateChannelsMixin, SizeMixin):
    def __init__(self, info, data, freqs, nave, comment=None,
                 method=None, verbose=None):
        if data.ndim < 2 or data.ndim > 3:
            raise ValueError('data should be 2d or 3d. Got %d.' % data.ndim)

        if data.ndim == 2:
            data = data[:, :, np.newaxis]

        n_channels, n_freqs, n_estimates = data.shape
        if n_channels != len(info['chs']):
            raise ValueError("Number of channels and data size don't match"
                             " (%d != %d)." % (n_channels, len(info['chs'])))
        if n_freqs != len(freqs):
            raise ValueError("Number of frequencies and data size don't match"
                             " (%d != %d)." % (n_freqs, len(freqs)))

        self.data = data
        self.info = info
        self.freqs = np.array(freqs, dtype=float)
        self.nave = nave
        self.comment = comment
        self.method = method
        self.preload = True

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    @property
    def ch_names(self):
        """Channel names."""
        return self.info['ch_names']

    def copy(self):
        """Return a copy of the instance.
        """
        return deepcopy(self)

    def freq2idx(self, freq):
        return np.abs(self.freqs - freq).argmin()

    def plot(self, picks=None, ax=None, proj=False,
             area_mode='std', area_alpha=0.33, show=True,
             color='black', xscale='linear', dB=True, estimate='auto',
             average=False, line_alpha=None, spatial_colors=True,
             sphere=None, verbose=None):
        from mne.viz.utils import _set_psd_plot_params, _plot_psd
        fig, picks_list, titles_list, units_list, scalings_list, ax_list, \
        make_label, xlabels_list = \
            _set_psd_plot_params(self.info, proj, picks, ax, area_mode)

        data = np.average(self.data, axis=2)

        psd_list = []
        for picks in picks_list:
            psd_list.append(data[picks, :])

        fig = _plot_psd(self, fig, self.freqs, psd_list, picks_list, titles_list,
                        units_list, scalings_list, ax_list, make_label, color,
                        area_mode, area_alpha, dB, estimate, average,
                        spatial_colors, xscale, line_alpha, sphere,
                        xlabels_list)

        plt_show(show)
        return fig




    def __repr__(self):  # noqa: D105
        s = "freq : [%f, %f]" % (self.freqs[0], self.freqs[-1])
        s += ", nave : %d" % self.nave
        s += ', channels : %d' % self.data.shape[0]
        s += ', ~%s' % (sizeof_fmt(self._size),)
        return "<Spectrum  |  %s>" % s
