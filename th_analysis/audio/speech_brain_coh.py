import mne
import numpy as np
from mne.io.constants import FIFF
from th_analysis.th_mne.spectrum import Spectrum
from mne.io.pick import _DATA_CH_TYPES_SPLIT


def calc_speech_brain_coherence(speech, brain, lag=0, window_length=2, fmin=2,
                                fmax=20, mode='fourier', conn_kwargs=None):
    if conn_kwargs is None:
        conn_kwargs = dict()

    brain = brain.copy()
    brain.shift_time(lag, True)
    brain.crop(0, speech.tmax)
    original_info = brain.info.copy()

    n_chans = len(brain.info['ch_names'])

    brain.add_channels([speech],
                       force_update_info=True)

    data_raw = mne.io.RawArray(
        np.squeeze(brain.get_data()),
        brain.info,
        verbose='WARNING'
    )

    events = mne.make_fixed_length_events(
        data_raw,
        duration=window_length,
        overlap=window_length/2,
    )

    data_final_epochs = mne.Epochs(
        data_raw,
        events,
        tmin=0,
        tmax=window_length,
        baseline=None
    )

    speech_chan_idx = n_chans

    indices = (
        np.ones((n_chans,), dtype=np.int) * speech_chan_idx,
        np.arange(0, n_chans)
    )
    coh_raw, freqs, times, n_epochs, n_tapers = mne.connectivity.spectral_connectivity(
        data_final_epochs,
        indices=indices,
        mode=mode,
        fmin=fmin,
        fmax=fmax,
        verbose='WARNING',
        **conn_kwargs
    )

    # %% put it into a class
    for (idx_chan, cur_chan) in enumerate(original_info['chs']):
        if mne.channel_type(original_info, idx_chan) in _DATA_CH_TYPES_SPLIT:
            cur_chan['unit'] = FIFF.FIFF_UNIT_NONE

    coh = Spectrum(original_info,
                   coh_raw,
                   freqs,
                   nave=len(data_final_epochs.events))

    return coh
