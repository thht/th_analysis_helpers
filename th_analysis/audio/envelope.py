from pydub import AudioSegment
import numpy as np
from chimera import equal_xbm_bands
import mne
from scipy.signal import hilbert


def extract_envelope(snd_file, startfreq=100, endfreq=10000, n_bands=9, pad_front=0, average_bands=True):
    snd = AudioSegment.from_file(snd_file)
    raw_data = np.array(snd.get_array_of_samples())
    snd_data = raw_data.reshape(-1, snd.channels)
    snd_data = snd_data.mean(axis=1)
    max_amp = np.max(np.abs(snd_data))
    snd_data = snd_data / max_amp

    bands = equal_xbm_bands(startfreq, endfreq, n_bands)

    all_envelopes = []
    start_freqs = bands[0:-1]
    end_freqs = bands[1:]
    n_pad_samples = int(np.ceil(pad_front * snd.frame_rate))

    for l_freq, h_freq in zip(start_freqs, end_freqs):
        filtered_data = mne.filter.filter_data(
            data=snd_data,
            sfreq=snd.frame_rate,
            l_freq=l_freq,
            h_freq=h_freq,
            method='iir',
            copy=True,
        )
        envelope = np.abs(hilbert(filtered_data))
        envelope = np.pad(envelope, (n_pad_samples, 0))
        all_envelopes.append(envelope)

    if average_bands:
        envelope = np.average(all_envelopes, axis=0)
        ch_names = ['audio_env']
    else:
        envelope = np.stack(all_envelopes, axis=0)
        ch_names = ['audio_env_%02d' % (idx+1) for idx in range(envelope.shape[0])]


    info = mne.create_info(
        ch_names,
        snd.frame_rate
    )

    if average_bands:
        env_tmp = envelope[np.newaxis, np.newaxis, ...]
    else:
        env_tmp = envelope[np.newaxis, ...]

    env_epochs = mne.EpochsArray(
        env_tmp,
        info,
        tmin=-pad_front
    )

    return env_epochs
