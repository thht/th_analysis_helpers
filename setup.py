# -*- coding: utf-8 -*-

import os.path
from codecs import open

from setuptools import setup, find_packages

REQUIRED = [
    'mne',
    'pydub',
    'chimera @ git+https://github.com/sbl/chimera.git#egg=chimera',
    'numpy',
    'scipy'
]

# find the location of this file
this_directory = os.path.abspath(os.path.dirname(__file__))

# Get the long description from the README file
with open(os.path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

version = '0.0.1'

setup(
    name='th_analysis_helpers',
    version=version,
    packages=find_packages(),
    url='https://gitlab.com/thht/th_analysis_helpers',
    license='GPLv3',
    author='Thomas Hartmann',
    author_email='thomas.hartmann@th-ht.de',
    description='Collection of M/EEG analysis helpers',
    long_description=long_description,
    long_description_content_type='text/markdown',
    install_requires=REQUIRED,
    include_package_data=True,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    keywords='MEG EEG'
)
